package by.achramionok.kirill.monitoringsystem.Container;

public class JWTContainer {
    private static String jwtToken;
    private static String currentUserName;

    public static String getCurrentUserName() {
        return currentUserName;
    }

    public static void setCurrentUserName(String currentUserName) {
        JWTContainer.currentUserName = currentUserName;
    }

    public static String getJwtToken() {
        return jwtToken;
    }

    public static void setJwtToken(String jwtToken) {
        JWTContainer.jwtToken = jwtToken;
    }
}

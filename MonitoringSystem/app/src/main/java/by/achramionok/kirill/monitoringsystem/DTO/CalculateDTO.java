package by.achramionok.kirill.monitoringsystem.DTO;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import by.achramionok.kirill.monitoringsystem.Entities.Event;

public class CalculateDTO {
    private List<TimeDTO> item;
    private Map<TimeDTO, List<Event>> itemCollections;

    public CalculateDTO() {
    }

    public CalculateDTO(List<TimeDTO> item, Map<TimeDTO, List<Event>> itemCollections) {
        this.item = item;
        this.itemCollections = itemCollections;
    }

    public List<TimeDTO> getItem() {
        return item;
    }

    public void setItem(List<TimeDTO> item) {
        this.item = item;
    }

    public Map<TimeDTO, List<Event>> getItemCollections() {
        return itemCollections;
    }

    public void setItemCollections(Map<TimeDTO, List<Event>> itemCollections) {
        this.itemCollections = itemCollections;
    }
}

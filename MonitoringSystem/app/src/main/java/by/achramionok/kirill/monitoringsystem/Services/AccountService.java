package by.achramionok.kirill.monitoringsystem.Services;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import by.achramionok.kirill.monitoringsystem.Container.JWTContainer;
import by.achramionok.kirill.monitoringsystem.Database.AppDatabase;
import by.achramionok.kirill.monitoringsystem.Entities.User;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AccountService {
    private OkHttpClient client;
    private static final MediaType JSON
            = MediaType.parse("application/json;");
    private static String url = "http://192.168.0.102:8080/api/account";
    private AppDatabase appDatabase;

    public AccountService(Context context) {
        client = new OkHttpClient();
        appDatabase = AppDatabase.getAppDatabase(context);
    }

    public void getUserAccount() throws IOException {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", JWTContainer.getJwtToken())
                .build();
        Response response = client.newCall(request).execute();
        if (response.body() != null){
            appDatabase.userDAO().insertAll(gson.fromJson(response.body().string(), User.class));
        }
    }
}

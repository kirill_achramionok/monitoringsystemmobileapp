package by.achramionok.kirill.monitoringsystem.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import by.achramionok.kirill.monitoringsystem.DAO.EventDAO;
import by.achramionok.kirill.monitoringsystem.DAO.EventTypeDAO;
import by.achramionok.kirill.monitoringsystem.DAO.UserDAO;
import by.achramionok.kirill.monitoringsystem.Entities.Event;
import by.achramionok.kirill.monitoringsystem.Entities.EventType;
import by.achramionok.kirill.monitoringsystem.Entities.User;

@Database(entities = {User.class, Event.class, EventType.class}, version = 1)
@TypeConverters({Converter.class})
public abstract class AppDatabase extends RoomDatabase {
    public static AppDatabase INSTANCE;

    public abstract UserDAO userDAO();

    public abstract EventDAO eventDAO();

    public abstract EventTypeDAO eventTypeDAO();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class, "monitor-database").allowMainThreadQueries().build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}

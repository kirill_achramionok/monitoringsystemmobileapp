package by.achramionok.kirill.monitoringsystem.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.sql.Date;
import java.time.Instant;
import java.util.List;

import by.achramionok.kirill.monitoringsystem.Entities.Event;

@Dao
public interface EventDAO {

    @Query("SELECT * FROM Event Order by eventdate DESC")
    List<Event> findAll();

    @Query("SELECT * FROM EVENT WHERE userId = :userId Order by eventdate DESC ")
    List<Event> findAllByUserId(Long userId);

    @Query("SELECT * FROM EVENT WHERE userId = :userId AND (eventdate >= :dateFrom and eventdate <= :dateTo) Order by eventdate ASC")
    List<Event> findAllByUserIdAndEventdateBetweenOrderByEventdateAsc(Long userId, Instant dateFrom, Instant dateTo);

    @Query("SELECT * FROM EVENT WHERE userId = :userId Order by eventdate DESC Limit 1")
    Event findLastByUserId(Long userId);

    @Query("SELECT * FROM EVENT WHERE userId = :userId and eventdate >= :date Order by eventdate ASC")
    List<Event> findTodayEventsByUserId(Long userId, Instant date);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Event... events);

    @Delete
    void delete(Event event);
}

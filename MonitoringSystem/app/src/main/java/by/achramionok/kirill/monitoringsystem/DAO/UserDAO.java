package by.achramionok.kirill.monitoringsystem.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import by.achramionok.kirill.monitoringsystem.Entities.User;

@Dao
public interface UserDAO {
    @Query("SELECT * FROM user")
    List<User> getAll();

    @Query("SELECT * FROM user where id = :userId")
    User findById(Long userId);

    @Query("SELECT * FROM user where login LIKE :login")
    User findByLogin(String login);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(User... users);

    @Delete
    void delete(User user);

    @Query("SELECT * FROM User")
    List<User> findAll();
}

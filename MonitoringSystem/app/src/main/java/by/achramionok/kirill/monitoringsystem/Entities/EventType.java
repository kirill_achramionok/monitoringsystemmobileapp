package by.achramionok.kirill.monitoringsystem.Entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "EventType")
public class EventType {

    @PrimaryKey(autoGenerate = false)
    private Long id;

    @ColumnInfo(name = "eventname")
    private String eventname;

    public EventType(Long id, String eventname) {
        this.id = id;
        this.eventname = eventname;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventname() {
        return eventname;
    }

    public void setEventname(String eventname) {
        this.eventname = eventname;
    }
}

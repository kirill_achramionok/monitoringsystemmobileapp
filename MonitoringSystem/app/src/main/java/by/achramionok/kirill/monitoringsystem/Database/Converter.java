package by.achramionok.kirill.monitoringsystem.Database;

import android.arch.persistence.room.TypeConverter;

import java.time.Instant;

public class Converter {
    @TypeConverter
    public Instant fromTimestamp(Long value) {
        return value == null ? null : Instant.ofEpochSecond(value);
    }

    @TypeConverter
    public Long instantToTimestamp(Instant instant) {
        if (instant == null) {
            return null;
        } else {
            return instant.getEpochSecond();
        }
    }
}

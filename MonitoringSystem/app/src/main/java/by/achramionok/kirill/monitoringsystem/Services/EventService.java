package by.achramionok.kirill.monitoringsystem.Services;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalField;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import by.achramionok.kirill.monitoringsystem.Container.JWTContainer;
import by.achramionok.kirill.monitoringsystem.DTO.CalculateDTO;
import by.achramionok.kirill.monitoringsystem.DTO.TimeDTO;
import by.achramionok.kirill.monitoringsystem.Database.AppDatabase;
import by.achramionok.kirill.monitoringsystem.Entities.Event;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class EventService {
    private OkHttpClient client;
    private static final MediaType JSON
            = MediaType.parse("application/json;");
    private static String url = "http://192.168.0.102:8080/api/events/by-user";
    private AppDatabase appDatabase;

    public EventService(Context context) {
        client = new OkHttpClient();
        appDatabase = AppDatabase.getAppDatabase(context);
    }

    public void findByUsername(String username) throws IOException {
        GsonBuilder builder = new GsonBuilder().registerTypeAdapter(Instant.class, new JsonDeserializer<Instant>() {
            @Override
            public Instant deserialize(JsonElement json, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
//                json.toString().substring(1, json.toString().length() - 1)
                Instant instant = Instant.ofEpochSecond(json.getAsLong());
//                return Instant.ofEpochSecond(json.getAsLong());
                return instant;
            }
        });
        Gson gson = builder.create();
        Request request = new Request.Builder()
                .url(url + "/" + username)
                .addHeader("Authorization", JWTContainer.getJwtToken())
                .build();
        Response response = client.newCall(request).execute();
        if (response.body() != null) {
            appDatabase.eventDAO().insertAll((Event[]) gson.fromJson(response.body().string(), new TypeToken<Event[]>() {
            }.getType()));
        }
    }

    public LocalTime countTimePerDay(List<Event> events) {
        LocalTime time = LocalTime.of(0, 0, 0);

        Event temp = null;
        for (Event event : events
                ) {
            if (appDatabase.eventTypeDAO().findById(event.getEventType()).getEventname().equals("Enter")) {
                if (events.size() != events.indexOf(event) + 1 && appDatabase.eventTypeDAO().findById((temp = events.get(events.indexOf(event) + 1)).getEventType()).getEventname().equals("Exit")) {
                    time = time.plus(ChronoUnit.SECONDS.between(event.getEventdate(), temp.getEventdate()), ChronoUnit.SECONDS);
                }
            }
        }
        return time;
    }

    public CalculateDTO getTimeSpentAtWorkByUserId(Long userId, Instant dateFrom, Instant dateTo) {
        CalculateDTO calculateDTO = new CalculateDTO();
        List<TimeDTO> timeDTOList = new ArrayList<>();
        Map<TimeDTO, List<Event>> map = new HashMap<>();
        long days = ChronoUnit.DAYS.between(dateFrom, dateTo);
        for (int i = 0; i < days; i++) {
            List<Event> events = appDatabase.eventDAO().findAllByUserIdAndEventdateBetweenOrderByEventdateAsc(
                    userId,
                    dateFrom,
                    dateFrom = dateFrom.plus(1, ChronoUnit.DAYS).minusSeconds(1)
            );
            if (events.size() != 0) {
                TimeDTO timeDTO = new TimeDTO(dateFrom.atZone(ZoneId.systemDefault()).toLocalDate(), countTimePerDay(events), LocalTime.of(8, 0, 0));
                timeDTOList.add(timeDTO);
                map.put(timeDTO, events);
            }
        }
        calculateDTO.setItem(timeDTOList);
        calculateDTO.setItemCollections(map);
        return calculateDTO;
    }
}

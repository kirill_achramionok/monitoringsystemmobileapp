package by.achramionok.kirill.monitoringsystem.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import by.achramionok.kirill.monitoringsystem.DTO.TimeDTO;
import by.achramionok.kirill.monitoringsystem.Database.AppDatabase;
import by.achramionok.kirill.monitoringsystem.Entities.Event;
import by.achramionok.kirill.monitoringsystem.R;

public class CustomExpandableListAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private Map<TimeDTO, List<Event>> itemCollections;
    private List<TimeDTO> item;

    public CustomExpandableListAdapter(Context context, Map<TimeDTO, List<Event>> itemCollections, List<TimeDTO> item) {
        this.mContext = context;
        this.itemCollections = itemCollections;
        this.item = item;
    }

    @Override
    public int getGroupCount() {
        return itemCollections.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return itemCollections.get(item.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return itemCollections.get(item.get(groupPosition));
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return itemCollections.get(item.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.group_view, null);
        }

        if (isExpanded) {
            //Изменяем что-нибудь, если текущая Group раскрыта
        } else {
            //Изменяем что-нибудь, если текущая Group скрыта
        }

        TextView textGroup = (TextView) convertView.findViewById(R.id.textGroup);
        textGroup.setText("Day: " +
                item.get(groupPosition).getDay().toString() +
                ", Time spent At work " +
                item.get(groupPosition).getTimeSpentAtWork() +
                ", Required time: " +
                item.get(groupPosition).getRequiredTime());

        return convertView;

    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.child_view, null);
        }

        TextView textChild = (TextView) convertView.findViewById(R.id.textChild);
        textChild.setText("Event date: " +
                itemCollections.get(item.get(groupPosition)).get(childPosition).getEventdate().toString() +
                ", Event type: " +
                AppDatabase.getAppDatabase(mContext).eventTypeDAO().findById(itemCollections.get(item.get(groupPosition)).get(childPosition).getEventType()).getEventname());

//        Button button = (Button)convertView.findViewById(R.id.buttonChild);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(mContext,"button is pressed",5000).show();
//            }
//        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}

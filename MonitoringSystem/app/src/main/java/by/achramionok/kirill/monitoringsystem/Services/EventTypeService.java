package by.achramionok.kirill.monitoringsystem.Services;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.time.Instant;
import java.util.List;

import by.achramionok.kirill.monitoringsystem.Container.JWTContainer;
import by.achramionok.kirill.monitoringsystem.Database.AppDatabase;
import by.achramionok.kirill.monitoringsystem.Entities.Event;
import by.achramionok.kirill.monitoringsystem.Entities.EventType;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class EventTypeService {
    private OkHttpClient client;
    private static final MediaType JSON
            = MediaType.parse("application/json;");
    private static String url = "http://192.168.0.102:8080/api/event-types";
    private AppDatabase appDatabase;

    public EventTypeService(Context context) {
        client = new OkHttpClient();
        appDatabase = AppDatabase.getAppDatabase(context);
    }

    public void getAllEventTypes() throws IOException {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", JWTContainer.getJwtToken())
                .build();
        Response response = client.newCall(request).execute();
        if (response.body() != null) {
            appDatabase.eventTypeDAO().insertAll((EventType[]) gson.fromJson(response.body().string(),
                    new TypeToken<EventType[]>() {
                    }.getType()));
        }
    }
}

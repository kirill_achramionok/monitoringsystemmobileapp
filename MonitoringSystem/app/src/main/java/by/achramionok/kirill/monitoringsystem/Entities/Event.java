package by.achramionok.kirill.monitoringsystem.Entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import java.time.Instant;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "Event", foreignKeys = {
        @ForeignKey(
                entity = User.class,
                parentColumns = "id",
                childColumns = "userId",
                onDelete = CASCADE),
        @ForeignKey(
                entity = EventType.class,
                parentColumns = "id",
                childColumns = "eventType",
                onDelete = CASCADE
        )
})

public class Event {
    @PrimaryKey(autoGenerate = false)
    private Long id;

    @ColumnInfo(name = "eventdate")
    private Instant eventdate;

    @ColumnInfo(name = "eventType", index = true)
    private Long eventType;

    @ColumnInfo(name = "userId", index = true)
    private Long userId;

    public Event(Long id, Instant eventdate, Long eventType, Long userId) {
        this.id = id;
        this.eventdate = eventdate;
        this.eventType = eventType;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getEventdate() {
        return eventdate;
    }

    public void setEventdate(Instant eventdate) {
        this.eventdate = eventdate;
    }

    public Long getEventType() {
        return eventType;
    }

    public void setEventType(Long eventType) {
        this.eventType = eventType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}

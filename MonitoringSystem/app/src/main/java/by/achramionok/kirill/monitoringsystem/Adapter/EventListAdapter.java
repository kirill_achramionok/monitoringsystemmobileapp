package by.achramionok.kirill.monitoringsystem.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Locale;

import by.achramionok.kirill.monitoringsystem.Database.AppDatabase;
import by.achramionok.kirill.monitoringsystem.Entities.Event;
import by.achramionok.kirill.monitoringsystem.R;

public class EventListAdapter extends ArrayAdapter<Event> {
    public EventListAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }

    public EventListAdapter(@NonNull Context context, int resource, List<Event> eventList) {
        super(context, resource, eventList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        DateTimeFormatter formatter =
                DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
                        .withLocale(Locale.forLanguageTag("RUSSIA"))
                        .withZone(ZoneId.systemDefault());
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.event_item, null);
        }

        Event item = getItem(position);
        TextView eventDate = (TextView) view.findViewById(R.id.event_date);
        TextView eventType = (TextView) view.findViewById(R.id.event_type);

        eventDate.setText("Event date: " + formatter.format(item.getEventdate()));
        eventType.setText("Event type: " + AppDatabase.getAppDatabase(getContext()).eventTypeDAO().findById(item.getEventType()).getEventname());

        return view;
    }
}

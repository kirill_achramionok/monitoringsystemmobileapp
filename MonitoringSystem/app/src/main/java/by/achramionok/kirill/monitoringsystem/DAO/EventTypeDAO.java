package by.achramionok.kirill.monitoringsystem.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import by.achramionok.kirill.monitoringsystem.Entities.EventType;

@Dao
public interface EventTypeDAO {
    @Query("SELECT * FROM EventType")
    List<EventType> getAll();

    @Query("SELECT * FROM EventType where id = :eventTypeId")
    EventType findById(Long eventTypeId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(EventType... eventTypes);

    @Query("SELECT * FROM EventType")
    List<EventType> findAll();
}

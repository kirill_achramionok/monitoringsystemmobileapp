package by.achramionok.kirill.monitoringsystem;

import android.content.Context;
import android.net.ProxyInfo;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import by.achramionok.kirill.monitoringsystem.Adapter.EventListAdapter;
import by.achramionok.kirill.monitoringsystem.Database.AppDatabase;
import by.achramionok.kirill.monitoringsystem.Entities.Event;
import by.achramionok.kirill.monitoringsystem.Entities.EventType;
import by.achramionok.kirill.monitoringsystem.Entities.User;
import by.achramionok.kirill.monitoringsystem.Services.EventService;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AccountFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccountFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_LOGIN = "login";

    // TODO: Rename and change types of parameters
    private TextView mEmail;
    private TextView mUserName;
    private ImageView mUserAvatar;
    private TextView mStatus;
    private TextView mTimeAtWork;
    private AppDatabase appDatabase;
    private OnFragmentInteractionListener mListener;
    private ListView todayEventsList;
    private List<Event> eventList;
    private EventService eventService;

    public AccountFragment() {
        // Required empty public constructor
        appDatabase = AppDatabase.getAppDatabase(getContext());
        eventService = new EventService(getContext());
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AccountFragment newInstance(String login) {
        AccountFragment fragment = new AccountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_LOGIN, login);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_account, container, false);
        mUserAvatar = (ImageView) view.findViewById(R.id.profileAvatar);
        mEmail = (TextView) view.findViewById(R.id.email);
        mUserName = (TextView) view.findViewById(R.id.username);
        mStatus = (TextView) view.findViewById(R.id.status);
        mTimeAtWork = (TextView) view.findViewById(R.id.time_at_work);
        todayEventsList = (ListView) view.findViewById(R.id.today_events);

        User user = appDatabase.userDAO().findByLogin(getArguments().getString(ARG_LOGIN));
        Event event = appDatabase.eventDAO().findLastByUserId(user.getId());
        EventType eventType = appDatabase.eventTypeDAO().findById(event.getEventType());
        eventList = appDatabase.eventDAO().findTodayEventsByUserId(user.getId(), LocalDate.now().atStartOfDay().toInstant(ZoneOffset.UTC));
        mTimeAtWork.setText("Time at work today: " + eventService.countTimePerDay(eventList).toString());
        mEmail.setText("Email: " + user.getEmail());
        mUserName.setText(user.getFirstName() + " " + user.getLastName());
        if (eventType.getEventname().equals("Enter")) {
            mStatus.setText("Status: At work!");
        } else {
            mStatus.setText("Status: Absent!");
        }
        new DownloadImageTask(mUserAvatar)
                .execute(user.getImageUrl());


        todayEventsList.setAdapter(new EventListAdapter(getContext(), R.layout.event_item, eventList));

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

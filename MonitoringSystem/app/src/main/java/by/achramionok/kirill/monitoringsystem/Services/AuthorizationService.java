package by.achramionok.kirill.monitoringsystem.Services;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;

import by.achramionok.kirill.monitoringsystem.Container.JWTContainer;
import by.achramionok.kirill.monitoringsystem.Database.AppDatabase;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AuthorizationService {
    private OkHttpClient client;
    private static final MediaType JSON
            = MediaType.parse("application/json;");
    private static String url = "http://192.168.0.102:8080/api/authenticate";


    public AuthorizationService() {
        client = new OkHttpClient();
    }

    public boolean login(String username, String password) throws IOException {
        RequestBody body = RequestBody.create(JSON, toJsonString(username, password));
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        JWTContainer.setJwtToken(response.header("Authorization"));
        JWTContainer.setCurrentUserName(username);
        return response.code() == 200;
    }

    private String toJsonString(String username, String password) {
        JSONObject object = new JSONObject();
        try {
            object.put("username", username);
            object.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object.toString();
    }
}

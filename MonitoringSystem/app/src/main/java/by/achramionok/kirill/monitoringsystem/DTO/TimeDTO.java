package by.achramionok.kirill.monitoringsystem.DTO;

import java.time.LocalDate;
import java.time.LocalTime;

public class TimeDTO {
    private LocalDate day;
    private LocalTime timeSpentAtWork;
    private LocalTime requiredTime;

    public TimeDTO() {
    }

    public TimeDTO(LocalDate day, LocalTime timeSpentAtWork, LocalTime requiredTime) {
        this.day = day;
        this.timeSpentAtWork = timeSpentAtWork;
        this.requiredTime = requiredTime;
    }

    public LocalDate getDay() {
        return day;
    }

    public void setDay(LocalDate day) {
        this.day = day;
    }

    public LocalTime getTimeSpentAtWork() {
        return timeSpentAtWork;
    }

    public void setTimeSpentAtWork(LocalTime timeSpentAtWork) {
        this.timeSpentAtWork = timeSpentAtWork;
    }

    public LocalTime getRequiredTime() {
        return requiredTime;
    }

    public void setRequiredTime(LocalTime requiredTime) {
        this.requiredTime = requiredTime;
    }
}

package by.achramionok.kirill.monitoringsystem;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;

import by.achramionok.kirill.monitoringsystem.Container.JWTContainer;
import by.achramionok.kirill.monitoringsystem.Database.AppDatabase;
import by.achramionok.kirill.monitoringsystem.Entities.Event;
import by.achramionok.kirill.monitoringsystem.Entities.User;
import by.achramionok.kirill.monitoringsystem.Services.AccountService;
import by.achramionok.kirill.monitoringsystem.Services.EventService;
import by.achramionok.kirill.monitoringsystem.Services.EventTypeService;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        AccountFragment.OnFragmentInteractionListener,
        CalculateFragment.OnFragmentInteractionListener,
        EventFragment.OnListFragmentInteractionListener {
    private static String LOGIN_KEY = "login";
    private static String JWT_KEY = "jwt";
    private AccountFragment accountFragment;
    private CalculateFragment calculateFragment;
    private FragmentTransaction fragmentTransaction;
    private EventFragment eventFragment;
    private User user;
    private AppDatabase appDatabase;
    private TextView userName;
    private ImageView userAvatar;

    private AccountService accountService;
    private EventService eventService;
    private EventTypeService eventTypeService;
    private String login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        login = getIntent().getStringExtra("login");
        appDatabase = AppDatabase.getAppDatabase(this);

        accountService = new AccountService(getApplicationContext());
        eventService = new EventService(getApplicationContext());
        eventTypeService = new EventTypeService(getApplicationContext());

        accountFragment = AccountFragment.newInstance(login);
        calculateFragment = CalculateFragment.newInstance(login);
        eventFragment = EventFragment.newInstance(1, login);

        user = appDatabase.userDAO().findByLogin(login);

        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, accountFragment);
        fragmentTransaction.commit();

//        List<Event> eventList = appDatabase.eventDAO().findAll();
//        List<EventType> eventTypeList = appDatabase.eventTypeDAO().findAll();
//        List<User> userList = appDatabase.userDAO().findAll();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoadDataTask loadDataTask = new LoadDataTask();
                loadDataTask.execute((Void) null);
                Snackbar.make(view, "Update data", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

//        userName = (TextView) findViewById(R.id.userName);
////        userAvatar = (ImageView) findViewById(R.id.userAvatar);
////
////        userName.setText(user.getFirstName());
////        new DownloadImageTask((ImageView) findViewById(R.id.userAvatar))
////                .execute(user.getImageUrl());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            SharedPreferences sharedPreferences = getSharedPreferences("credentials", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove(LOGIN_KEY);
            editor.remove(JWT_KEY);
            editor.commit();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        fragmentTransaction = getFragmentManager().beginTransaction();
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            fragmentTransaction.replace(R.id.fragmentContainer, accountFragment);
        } else if (id == R.id.nav_gallery) {
            fragmentTransaction.replace(R.id.fragmentContainer, eventFragment);
        } else if (id == R.id.nav_slideshow) {
            fragmentTransaction.replace(R.id.fragmentContainer, calculateFragment);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        fragmentTransaction.commit();
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onListFragmentInteraction(Event item) {

    }

    public class LoadDataTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
//                fragmentTransaction = getFragmentManager().beginTransaction();
                accountService.getUserAccount();
                eventTypeService.getAllEventTypes();
                eventService.findByUsername(JWTContainer.getCurrentUserName());
//                List<Fragment> fragments = getFragmentManager().getFragments();
//                for (Fragment fragment : fragments
//                        ) {
//                    if (fragment instanceof AccountFragment) {
//                        fragmentTransaction.detach(accountFragment);
//                        fragmentTransaction.attach(accountFragment = AccountFragment.newInstance(login));
//                    } else if (fragment instanceof CalculateFragment) {
//                        fragmentTransaction.detach(calculateFragment);
//                        fragmentTransaction.attach(calculateFragment = new CalculateFragment());
//                    } else if (fragment instanceof EventFragment) {
//                        fragmentTransaction.detach(eventFragment);
//                        fragmentTransaction.attach(eventFragment = EventFragment.newInstance(1, login));
//                    }
//                }
//                fragmentTransaction.commit();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {

        }
    }
}
